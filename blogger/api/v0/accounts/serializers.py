import json
import random
import string

import requests
from django.core.mail import send_mail
from grab import Grab
from rest_framework import serializers
from rest_framework.authtoken.models import Token

from accounts.models import User, Blogger, HashTag, YouTubeInfo, Location, CodeForChangePassword, InstagramInfo
from core.settings.base import API_KEY_YOUTUBE, DEFAULT_FROM_EMAIL
from statistic.models import BloggerViewStatistic


class LocationSerializer(serializers.ModelSerializer):
    class Meta:
        model = Location
        fields = 'country', 'region', 'city'


class LocationRegistrationSerializer(serializers.Serializer):
    country = serializers.CharField(write_only=True, required=False, allow_blank=True, )
    region = serializers.CharField(write_only=True, required=False, allow_blank=True, )
    city = serializers.CharField(write_only=True, required=False, allow_blank=True, )


class BloggerRegistrationSerializer(serializers.Serializer):
    """
    Serializer for registration blogger
    """
    email = serializers.EmailField()
    login = serializers.CharField()
    password = serializers.CharField(write_only=True)
    category = serializers.CharField(write_only=True)
    location = LocationRegistrationSerializer(write_only=True)
    link = serializers.URLField(write_only=True)
    link_key = serializers.CharField(write_only=True, required=False, allow_blank=True)
    type = serializers.CharField(write_only=True)
    condition = serializers.CharField(write_only=True)
    contact = serializers.CharField(write_only=True)
    tags = serializers.ListField(required=False, write_only=True)

    def create(self, validated_data):
        user = User(email=validated_data.get('email'), login=validated_data.get('login'))
        user.set_password(validated_data.get('password'))
        # TODO remove this after do activation email
        user.is_active = True
        user.save()
        blogger = Blogger.objects.create(user=user, category=validated_data.get('category'),
                                         link=validated_data.get('link'), link_key=validated_data.get('link_key'),
                                         type=validated_data.get('type'), condition=validated_data.get('condition'),
                                         contact=validated_data.get('contact'))
        tags = validated_data.get('tags')
        if tags:
            for tag in tags:
                HashTag.objects.create(user=user, title=tag)
        location_info = validated_data.get('location')
        location = Location.objects.create(city=location_info.get('city'), region=location_info.get('region'),
                                           country=location_info.get('country'), blogger=blogger)

        if validated_data.get('type') == 'youtube':
            url = 'https://www.googleapis.com/youtube/v3/' \
                  'channels?part=snippet%2' \
                  'CcontentDetails%2' \
                  'Cstatistics&key=' + API_KEY_YOUTUBE + '&id=' + validated_data.get('link_key')
            response = requests.get(url)
            try:
                r = response.json()
                subscriber_count = r['items'][0]['statistics']['subscriberCount']
                video_count = r['items'][0]['statistics']['videoCount']
                view_count = r['items'][0]['statistics']['viewCount']
                youtube_info = YouTubeInfo(blogger=blogger, info_json=response.text,
                                           subscriber_count=subscriber_count,
                                           video_count=video_count, view_count=view_count)
                youtube_info.save()
            except Exception:
                url = 'https://www.googleapis.com/youtube/v3/' \
                      'channels?part=snippet%2' \
                      'CcontentDetails%2' \
                      'Cstatistics&key=' + API_KEY_YOUTUBE + '&forUsername=' + validated_data.get('link_key')
                response = requests.get(url)
                try:
                    r = response.json()
                    subscriber_count = r['items'][0]['statistics']['subscriberCount']
                    video_count = r['items'][0]['statistics']['videoCount']
                    view_count = r['items'][0]['statistics']['viewCount']
                    youtube_info = YouTubeInfo(blogger=blogger, info_json=response.text,
                                               subscriber_count=subscriber_count,
                                               video_count=video_count, view_count=view_count)
                    youtube_info.save()
                except Exception:
                    user.delete()
                    raise serializers.ValidationError("Нет информации о данном youtube канале")
        elif validated_data.get('type') == 'instagram':
            try:
                g = Grab()
                url = validated_data.get('link').split("?")
                g.go(url[0])
                str_doc = g.doc.unicode_body()
                import re
                m = re.findall(
                    r'meta property="og:description" content="(?P<fs>.*) Followers, (?P<fl>.*) Following, (?P<po>.*) Posts',
                    str_doc)
                followers = m[0][0]
                following = m[0][1]
                posts = m[0][2]
                InstagramInfo.objects.create(blogger=blogger, followers=followers, following=following, posts=posts)
            except Exception:
                user.delete()
                raise serializers.ValidationError("Нет информации о данном instagram канале")
        else:
            user.delete()
            raise serializers.ValidationError("Передан неправильный параметер тип")
        # TODO activation email
        user.send_activate_code_by_email()
        return user

    def update(self, instance, validated_data):
        pass

    def validate_email(self, email):
        """
        Check is user with this email exist
        """
        existing = User.objects.filter(email=email).first()
        if existing:
            raise serializers.ValidationError("Пользователь с данным email существует.")
        return email

    def validate_login(self, login):
        """
        Check is user with this login exist
        """
        existing = User.objects.filter(login=login).first()
        if existing:
            raise serializers.ValidationError("Пользователь с данным логином существует.")
        return login

    def validate_link(self, link):
        """
        Check is user with this link exist
        """
        existing = Blogger.objects.filter(link=link).first()
        if existing:
            raise serializers.ValidationError("Пользователь с данным link существует.")
        return link


class LoginByEmailSerializer(serializers.Serializer):
    """
    Serializer for login or email
    """
    login = serializers.CharField()
    password = serializers.CharField()

    def validate_login(self, login):
        """
        Check is user with this login exist
        """
        existing = User.objects.filter(login__icontains=login.lower()).first()
        if not existing:
            existing_email = User.objects.filter(email__icontains=login.lower()).first()
            if not existing_email:
                raise serializers.ValidationError("User with this login doesn't exist")
        return login

    def validate(self, data):
        """
        Check is password correct
        """
        password = data.get('password')
        login = data.get('login')
        try:
            user = User.objects.get(login__icontains=login.lower())
        except User.DoesNotExist:
            user = User.objects.filter(email__icontains=login.lower()).first()
            if not user:
                raise serializers.ValidationError("Пользователь с данным логином не существует")
        if not user.check_password(password):
            raise serializers.ValidationError("Пароль неправильный")
        if not user.is_active:
            raise serializers.ValidationError("Пользователь не активный")
        return data

    def sign_in(self):
        """
        This method will return user token
        """
        user = User.objects.filter(login__icontains=self.validated_data.get('login').lower()).first()
        if not user:
            user = User.objects.filter(email__icontains=self.validated_data.get('login').lower()).first()
        token = Token.objects.get(user=user)
        return token.key


class InstagramSerializer(serializers.ModelSerializer):
    class Meta:
        model = InstagramInfo
        fields = 'followers', 'following', 'posts'


class YouTubeInfoSerializer(serializers.ModelSerializer):
    name = serializers.SerializerMethodField()
    image_url = serializers.SerializerMethodField()

    class Meta:
        model = YouTubeInfo
        fields = 'subscriber_count', 'video_count', 'view_count', 'name', 'image_url'

    def get_name(self, obj):
        r = json.loads(obj.info_json)
        return r['items'][0]['snippet']['title']

    def get_image_url(self, obj):
        r = json.loads(obj.info_json)
        return r['items'][0]['snippet']['thumbnails']['medium']


class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = 'login', 'email',


class BloggerSerializer(serializers.ModelSerializer):
    youtube_info = YouTubeInfoSerializer()
    location = LocationSerializer()
    user = UserSerializer()
    instagram = InstagramSerializer()
    views = serializers.SerializerMethodField()

    class Meta:
        model = Blogger
        fields = 'pk', 'category', 'link', 'type', 'condition', 'contact', \
                 'youtube_info', 'user', 'location', 'instagram', 'views'

    def get_views(self, obj):
        info = BloggerViewStatistic.objects.filter(blogger=obj)
        return len(info)

class BloggerInfoSerializer(serializers.ModelSerializer):
    """Using for next serializer"""
    location = LocationSerializer()

    class Meta:
        model = Blogger
        fields = 'pk', 'category', 'link', 'type', 'condition', 'contact', 'user', 'location'


class UserInfoSerializer(serializers.ModelSerializer):
    blogger = BloggerInfoSerializer()

    class Meta:
        model = User
        fields = 'login', 'email', 'blogger'


class SendCodeSerializer(serializers.Serializer):
    email = serializers.EmailField()

    def send_code(self):
        email = self.validated_data.get('email')
        user = User.objects.filter(email=email).last()
        if user:
            code = ''.join([random.choice(string.ascii_letters + string.digits) for n in range(20)])
            code_for_change_password = CodeForChangePassword.objects.filter(user=user).last()
            if code_for_change_password:
                code_for_change_password.code = code
                code_for_change_password.save()
            else:
                CodeForChangePassword.objects.create(user=user, code=code)
            send_mail(
                'Blogger: Востановления пароля',
                'Ваш код для востановленния пароля: %s' % code,
                '%s' % DEFAULT_FROM_EMAIL,
                ['%s' % email],
                fail_silently=False,
            )
        else:
            raise serializers.ValidationError("Пользователь с данным email не найден")


class ChangePasswordSerializer(serializers.Serializer):
    code = serializers.CharField(max_length=100)
    password = serializers.CharField(max_length=100)

    def change_password(self):
        code = self.validated_data.get('code')
        password = self.validated_data.get('password')
        code_for_change_password = CodeForChangePassword.objects.filter(code=code).last()
        if code_for_change_password:
            user = code_for_change_password.user
            user.set_password(password)
            user.save()
            code_for_change_password.delete()
        else:
            raise serializers.ValidationError("Неправильный код")


class BloggerUpdateSerializer(serializers.Serializer):
    """
    Update blogger
    """

    password = serializers.CharField(write_only=True, required=False)
    category = serializers.CharField(write_only=True, required=False)
    location = LocationRegistrationSerializer(write_only=True, required=False)
    condition = serializers.CharField(write_only=True, required=False)
    contact = serializers.CharField(write_only=True, required=False)
    tags = serializers.ListField(required=False, write_only=True)

    def custom_update(self, blogger):
        user = blogger.user
        password = self.validated_data.get('password')
        category = self.validated_data.get('category')
        condition = self.validated_data.get('condition')
        contact = self.validated_data.get('condition')
        location = self.validated_data.get('location')
        tags = self.validated_data.get('location')
        if password:
            user.set_password(password)
        user.save()
        if category:
            blogger.category = category
        if condition:
            blogger.condition = condition
        if contact:
            blogger.contact = contact
        blogger.save()
        if location:
            Location.objects.filter(blogger=blogger).update(city=location.get('city'),
                                                            region=location.get('region'),
                                                            country=location.get('country'),)
        if tags:
            HashTag.objects.filter(user=user).delete()
            for tag in tags:
                HashTag.objects.create(user=user, title=tag)

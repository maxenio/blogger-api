import requests
from grab import Grab
from rest_framework import generics, permissions, status
from rest_framework.response import Response

from accounts.models import Blogger
from api.v0.accounts.serializers import BloggerRegistrationSerializer, LoginByEmailSerializer, UserSerializer, \
    BloggerSerializer, SendCodeSerializer, ChangePasswordSerializer, BloggerUpdateSerializer, UserInfoSerializer
from statistic.models import BloggerViewStatistic


class BloggerView(generics.CreateAPIView):
    """
    View for register user
    """
    serializer_class = BloggerRegistrationSerializer
    permission_classes = (permissions.AllowAny,)


class LoginView(generics.GenericAPIView):
    """
    View for login user by email
    """
    serializer_class = LoginByEmailSerializer
    permission_classes = (permissions.AllowAny,)

    def post(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        key = serializer.sign_in()
        return Response({'token': key}, status=status.HTTP_200_OK)


class UserView(generics.RetrieveAPIView):
    """
    Get info for user
    """
    serializer_class = UserInfoSerializer
    permission_classes = (permissions.AllowAny,)

    def get_object(self):
        obj = self.request.user
        return obj


class BloggerListView(generics.ListAPIView):
    """
    Blogger list
    """
    serializer_class = BloggerSerializer
    permission_classes = (permissions.AllowAny,)
    queryset = Blogger.objects.filter(user__is_active=True)

    def get_queryset(self):
        if 'type' in self.request.GET:
            queryset = Blogger.objects.filter(user__is_active=True, type=self.request.GET.get('type'))
        else:
            queryset = Blogger.objects.filter(user__is_active=True)
        return queryset


class BloggerDetailView(generics.RetrieveAPIView):
    """
    Blogger detail
    """
    serializer_class = BloggerSerializer
    permission_classes = (permissions.AllowAny,)
    queryset = Blogger.objects.filter(user__is_active=True)

    def get(self, request, *args, **kwargs):
        BloggerViewStatistic.objects.create(blogger=self.get_object())
        return self.retrieve(request, *args, **kwargs)


class ChoiceTypeBlogger(generics.GenericAPIView):
    """
    Choices for type blogger
    """
    serializer_class = BloggerSerializer
    permission_classes = (permissions.AllowAny,)

    def get(self, request, *args, **kwargs):
        dict_choices = {'youtube': 'youtube', 'instagram': 'instagram'}
        return Response(dict_choices, status=status.HTTP_200_OK)


class SendCodeForChangePassword(generics.GenericAPIView):
    """
    Send code for change password
    """
    serializer_class = SendCodeSerializer
    permission_classes = (permissions.AllowAny,)

    def post(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.send_code()
        return Response(status=status.HTTP_201_CREATED)


class ChangePasswordView(generics.GenericAPIView):
    """
    Send code for change password
    """
    serializer_class = ChangePasswordSerializer
    permission_classes = (permissions.AllowAny,)

    def post(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.change_password()
        return Response(status=status.HTTP_200_OK)


class UpdateBloggerView(generics.GenericAPIView):
    """
    Update blogger
    """
    serializer_class = BloggerUpdateSerializer
    permission_classes = (permissions.IsAuthenticated,)

    def get_object(self):
        user = self.request.user
        blogger = Blogger.objects.get(user=user)
        return blogger

    def patch(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.custom_update(self.get_object())
        return Response(status=status.HTTP_200_OK)

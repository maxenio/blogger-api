from django.conf.urls import url, include
from api.v0.accounts import views as accounts_views


urlpatterns = [
    ### Accounts urls ###
    url(r'^blogger/$', accounts_views.BloggerView.as_view(), name='blogger'),
    url(r'^blogger/(?P<pk>\d+)$', accounts_views.BloggerDetailView.as_view(), name='blogger_detail'),
    url(r'^blogger/update/$', accounts_views.UpdateBloggerView.as_view(), name='blogger_update'),
    url(r'^blogger/list/$', accounts_views.BloggerListView.as_view(), name='blogger_list'),
    url(r'^user/log_in/$', accounts_views.LoginView.as_view(), name='log_in'),
    url(r'^user/$', accounts_views.UserView.as_view(), name='user'),
    url(r'^user/send_code/$', accounts_views.SendCodeForChangePassword.as_view(), name='send_code'),
    url(r'^user/change_password/$', accounts_views.ChangePasswordView.as_view(), name='change_password'),

    ### Choices ###
    url(r'^choices/blogger/type$', accounts_views.ChoiceTypeBlogger.as_view(), name='choices_blogger_type'),

]

from django.contrib import admin

from accounts.models import Blogger, HashTag, YouTubeInfo, ActivationCode, User, Location, InstagramInfo

admin.site.register(Blogger)
admin.site.register(HashTag)
admin.site.register(User)
admin.site.register(YouTubeInfo)
admin.site.register(ActivationCode)
admin.site.register(Location)
admin.site.register(InstagramInfo)

import random
import string

from django.conf import settings
from django.contrib.auth.base_user import AbstractBaseUser, BaseUserManager
from django.core.mail import send_mail
from django.db import models
from django.db.models.signals import post_save
from django.dispatch import receiver
from rest_framework.authtoken.models import Token

from core.settings.base import DEFAULT_FROM_EMAIL, DOMAIN

TYPE_BLOGGER = (
    ('youtube', 'youtube'),
    ('instagram', 'instagram'),
)


class MyUserManager(BaseUserManager):
    def create_user(self, email, login, password=None):
        """
        Creates and saves a User with the given email, login
        """
        if not email:
            raise ValueError('Users must have an email address')

        user = self.model(
            login=login,
            email=email,
        )

        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_superuser(self, email, login, password):
        """
        Creates and saves a superuser with the given email
        """
        user = self.create_user(
            email,
            login,
            password=password,
        )
        user.is_admin = True
        user.is_active = True
        user.save(using=self._db)
        return user


class User(AbstractBaseUser):
    email = models.EmailField(
        verbose_name='email address',
        max_length=255,
        unique=True,
    )
    login = models.CharField(max_length=100, unique=True)
    is_active = models.BooleanField(default=False)
    is_admin = models.BooleanField(default=False)
    created_date = models.DateTimeField(auto_now_add=True)

    objects = MyUserManager()

    USERNAME_FIELD = 'login'

    REQUIRED_FIELDS = 'email',

    def get_full_name(self):
        # The user is identified by their email address
        return self.email

    def get_short_name(self):
        # The user is identified by their email address
        return self.email

    def __str__(self):  # __unicode__ on Python 2
        return self.email

    def has_perm(self, perm, obj=None):
        "Does the user have a specific permission?"
        # Simplest possible answer: Yes, always
        return True

    def has_module_perms(self, app_label):
        "Does the user have permissions to view the app `app_label`?"
        # Simplest possible answer: Yes, always
        return True

    @property
    def is_staff(self):
        "Is the user a member of staff?"
        # Simplest possible answer: All admins are staff
        return self.is_admin

    def send_activate_code_by_email(self):
        """
        Send code for activate account
        :return:
        """
        code, created = ActivationCode.objects.update_or_create(
            defaults={"user": self}
        )
        send_mail(
            'Blogger Activate Your Account',
            'Activation link: %s/%s' % (DOMAIN, code.code),
            '%s' % DEFAULT_FROM_EMAIL,
            ['%s' % self.email],
            fail_silently=False,
        )


class Blogger(models.Model):
    """
    Bloger prototype
    """
    user = models.OneToOneField(User, verbose_name='Пользователь', related_name='blogger')
    category = models.CharField(max_length=255, verbose_name='Категория')
    link = models.CharField(max_length=255, unique=True, verbose_name='Ссылка на канал')
    link_key = models.CharField(max_length=255, blank=True, null=True)
    type = models.CharField(max_length=255, choices=TYPE_BLOGGER)
    condition = models.TextField(blank=True, null=True)
    contact = models.CharField(max_length=255)

    def __str__(self):
        return self.user.login


class HashTag(models.Model):
    """
    Model for save hash tag for blogger
    """
    title = models.CharField(max_length=255, verbose_name='Хеш тег')
    user = models.ForeignKey(User, )

    def __str__(self):
        return self.title


@receiver(post_save, sender=settings.AUTH_USER_MODEL)
def create_auth_token(sender, instance=None, created=False, **kwargs):
    """
    This code is triggered whenever a new user has been created and saved to the database
    """
    if created:
        Token.objects.create(user=instance)


class YouTubeInfo(models.Model):
    """
    Save data from YouTube API
    """
    blogger = models.OneToOneField(Blogger, related_name='youtube_info')
    info_json = models.TextField()
    subscriber_count = models.IntegerField()
    video_count = models.IntegerField()
    view_count = models.IntegerField()

    def __str__(self):
        return '%s' % self.blogger.user.email


class ActivationCode(models.Model):
    """
    Save code for activation account
    """
    code = models.CharField(max_length=500)
    user = models.OneToOneField(User)

    def __str__(self):
        return '%s' % self.user.email

    def save(self, *args, **kwargs):
        self.code = ''.join([random.choice(string.ascii_letters + string.digits) for n in range(32)])
        super(ActivationCode, self).save(*args, **kwargs)


class CodeForChangePassword(models.Model):
    """
    Save code for activation account
    """
    code = models.CharField(max_length=20)
    user = models.OneToOneField(User)

    def __str__(self):
        return '%s' % self.user.email


class Location(models.Model):
    """
    Save geo info for blogger
    """
    blogger = models.OneToOneField(Blogger, related_name='location')
    country = models.CharField(null=True, blank=True, max_length=255, verbose_name='Страна')
    region = models.CharField(null=True, blank=True, max_length=255, verbose_name='Регион')
    city = models.CharField(null=True, blank=True, max_length=255, verbose_name='Город')

    def __str__(self):
        return '%s' % self.blogger.user.email


class InstagramInfo(models.Model):
    """
    Save geo info for blogger
    """
    blogger = models.OneToOneField(Blogger, related_name='instagram')
    followers = models.CharField(null=True, blank=True, max_length=255, verbose_name='Подписчики')
    following = models.CharField(null=True, blank=True, max_length=255, verbose_name='Подписанный')
    posts = models.CharField(null=True, blank=True, max_length=255, verbose_name='Посты')

    def __str__(self):
        return '%s' % self.blogger.user.email

# -*- coding: utf-8 -*-
# Generated by Django 1.11.7 on 2018-01-28 16:16
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('accounts', '0014_codeforchangepassword'),
    ]

    operations = [
        migrations.CreateModel(
            name='InstagramInfo',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('followers', models.CharField(blank=True, max_length=255, null=True, verbose_name='Подписчики')),
                ('following', models.CharField(blank=True, max_length=255, null=True, verbose_name='Подписанный')),
                ('posts', models.CharField(blank=True, max_length=255, null=True, verbose_name='Посты')),
                ('blogger', models.OneToOneField(on_delete=django.db.models.deletion.CASCADE, related_name='instagram', to='accounts.Blogger')),
            ],
        ),
    ]

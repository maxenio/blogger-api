from django.db import models

from accounts.models import Blogger


class BloggerViewStatistic(models.Model):
    created_date = models.DateField(auto_now_add=True)
    blogger = models.ForeignKey(Blogger)

    def __str__(self):
        return '%s' % self.blogger.user.email

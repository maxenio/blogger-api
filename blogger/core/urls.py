from django.conf.urls import include, url
from django.contrib import admin
from django.views.debug import default_urlconf
from rest_framework_swagger.views import get_swagger_view

schema_view = get_swagger_view(title='Blogger API')

urlpatterns = [
    url(r'^$', default_urlconf),
    url(r'^docs/$', schema_view),
    url(r'^api/v0/', include('api.v0.urls', namespace='api_v0')),
    url(r'^admin/', include(admin.site.urls)),
]

